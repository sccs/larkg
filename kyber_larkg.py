from kyber_skem import KyberSKEM, Kyber, DEFAULT_PARAMETERS
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from collections import namedtuple
from random import uniform
from operator import mul
from functools import reduce
import time


PublicParams = namedtuple('PublicParams', 'skem kdf_1 kdf_2 rsp rsp_dom M')
Cred = namedtuple('Cred', 'B_prime c mu')


def _hkdf(msg):
    kdf = HKDF(
        algorithm=hashes.SHA256(),
        length=32,
        salt=None,
        info=None,
        backend=default_backend())
    return kdf.derive(msg)


''' Invert Montgomery and flatten. '''
def _from_montgomery_to_flat(pp, poly):
    def inverse(elem):
        return elem * pp.skem.R.ntt_helper.mont_r_inv % pp.skem.q

    return [v - pp.skem.q if (v := inverse(e)) >= pp.skem.q - 2*pp.skem.eta_1
            else v for row in poly for col in row for e in col]


def setup(skem, rsp, rsp_dom, repetition_rate):
    kdf_1 = lambda K: skem._generate_error_vector(K, skem.eta_1, 0)[0]
    return PublicParams(skem, kdf_1, _hkdf, rsp, rsp_dom, repetition_rate)


def keygen(pp):
    return pp.skem.keygen_dec()


def derive_pk(pp, pk):
    B = pk
    B_poly = pp.skem.M.decode(B, pp.skem.k, 1, l=12, is_ntt=True)

    B_prime, S_prime = pp.skem.keygen_enc()                              # Ln 1
    c, k_seed = pp.skem.encaps(S_prime, B)                               # Ln 2

    K = pp.kdf_1(k_seed)                                                 # Ln 3
    K.to_ntt()

    mu = pp.kdf_2(k_seed)                                                # Ln 4

    E_prime = pp.skem._generate_error_vector(pp.skem.random_bytes(32),   # Ln 5
                                             pp.skem.eta_1, 0)[0]
    E_prime.to_ntt()


    P = (pp.skem.A @ K).to_montgomery() + E_prime + B_poly               # Ln 6
    P.reduce_coefficents()
    return P.encode(l=12), Cred(B_prime, c, mu)                          # Ln 7


def derive_sk(pp, sk, cred):
    B_prime, c, mu = cred
    S = sk
    S_poly = pp.skem.M.decode(S, pp.skem.k, 1, l=12, is_ntt=True)

    S_prime_prime = False                                                # Ln 1
    k_seed = pp.skem.decaps(S, c, B_prime)                               # Ln 2

    K = pp.kdf_1(k_seed)                                                 # Ln 3
    K.to_ntt()

    mu_star = pp.kdf_2(k_seed)                                           # Ln 4

    if mu_star == mu:                                                    # Ln 5
        S_prime_prime = S_poly + K                                       # Ln 6
        S_prime_prime.reduce_coefficents()
        S_prime_prime_encoded = S_prime_prime.encode(l=12)
    else:
        return False  # Abort early as chi_b_S_prime_prime otherwise undefined

    u = uniform(0, 1)  # NOT CRYPTOGRAPHICALLY SECURE                    # Ln 7

    S_poly.from_ntt()
    S_flat = _from_montgomery_to_flat(pp, S_poly)

    S_prime_prime.from_ntt()
    S_prime_prime_flat = _from_montgomery_to_flat(pp, S_prime_prime)

    chi_a_S = [pp.rsp[abs(elem)]/pp.rsp_dom for elem in S_flat]
    chi_b_S_prime_prime = [pp.rsp[abs(elem - S_flat[i])]/pp.rsp_dom for i, elem
                           in enumerate(S_prime_prime_flat)]

    reject_value = reduce(mul, map(lambda pair: pair[1] / pair[0],
                                   zip(chi_a_S, chi_b_S_prime_prime)), 1)

    if u < reject_value * pp.M:
        return False                                                     # Ln 8

    return S_prime_prime_encoded                                         # Ln 9


def test(pp, skp, pkp):
    if not skp:
        return False

    pkp = pkp + pp.skem.rho
    m = b' '*32
    c = pp.skem._cpapke_enc(pkp, m, pp.skem.random_bytes(32))
    mm = pp.skem._cpapke_dec(skp, c)
    return m == mm


if __name__ == '__main__':
    pp = setup(KyberSKEM(DEFAULT_PARAMETERS['kyber_1024']),
               rsp=[6, 4, 1, 0, 0], rsp_dom=16,
               repetition_rate=162_755)
    computed_in = []
    for i in range(1000):
        count = 0
        skp = False
        while not skp:
            pk, sk = keygen(pp)
            pkp, cred = derive_pk(pp, pk)
            skp = derive_sk(pp, sk, cred)
            count += 1
        computed_in.append(count)
        print(i)
    print(sum(computed_in))
        #assert test(pp, skp, pkp)
    print(rejects)
